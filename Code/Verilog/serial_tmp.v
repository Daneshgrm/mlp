module serial_tmp( RX, TX, OUT, IN, CLOCK, RESET, START, RDY, IN_RDY
    );

	parameter DIVISOR = 4;
	localparam bitWidth = 16;
	localparam packetNo = 2;
	
	input wire CLOCK;
	input wire RESET;
	input wire START;
	input wire RX;
	input wire [bitWidth-1:0]	IN;
	
	output wire TX;
	output reg RDY;
	output reg IN_RDY;
	output reg [bitWidth-1:0]	OUT;
	
	parameter T_IDLE = 1'b0,	T_TRANS = 1'b1; 
	parameter R_IDLE = 1'b0,	R_REC	=	1'b1;
	reg	TCS, TNS;
	reg	RCS, RNS;
	reg	TX_enable;
	reg	[7:0]	TX_data;
	wire			TX_ready;
	
	wire	[7:0]	RX_data;
	wire			RX_enable;
	
	reg	[1:0]	T_counter;
	reg	[1:0]	R_counter;
	wire			start_trans;
	
	
	assign start_trans = START & TX_ready;
	
	basic_uart #(.DIVISOR(DIVISOR))myUART1(.clk(CLOCK), .reset(~RESET), 
			.rx(RX), .rx_data(RX_data), .rx_enable(RX_enable),
			.tx_data(TX_data), .tx_enable(TX_enable), 
			.tx(TX), .tx_ready(TX_ready));


	always@(posedge CLOCK)	begin
		if(~RESET)	begin
			T_counter <= 0;
			R_counter <= 0;
			RDY <= 0;
			IN_RDY <= 1'b1;
		end
	end
	
	always@(TCS, T_counter, start_trans, TX_ready)	begin
		case(TCS)
			T_IDLE:	begin
				if(start_trans && T_counter < packetNo)	begin
						TNS = T_TRANS;
						TX_data = IN[T_counter*8 +: 8];
						IN_RDY = ~(T_counter == packetNo-1);
						TX_enable = 1'b1;
				end
				else if(T_counter == packetNo)	begin
						TNS	= T_IDLE;
						IN_RDY = 1'b1;
						T_counter = 0;
				end
				else	begin
						TNS = T_IDLE;
				end
			end
			T_TRANS:	begin
						TX_enable = 1'b0;
					if(TX_ready)	begin
						TNS = T_IDLE;
						T_counter = T_counter + 1;
					end
					else	begin
						TNS = T_TRANS;
					end
			end
			default:
						TNS = T_IDLE;
		endcase
	end
	
	always@(posedge CLOCK)	begin
		if(~RESET)
			TCS <= T_IDLE;
		else
			TCS <= TNS;
	end
	
	always@(RCS, RX_enable, R_counter)	begin
		case(RCS)
			R_IDLE:	begin
				if(RX_enable && R_counter < packetNo)	begin
					RNS		=	R_REC;
					OUT[R_counter*8 +:8]	= RX_data;
				end
				else if(R_counter == packetNo)	begin
					R_counter = 0;
					RNS = R_IDLE;
					RDY = 1'b1;
				end
				else	begin
					RNS = R_IDLE;
				end	
			end
			R_REC:	begin
					RDY = 1'b0;
					if(~RX_enable)	begin
						RNS = R_IDLE;
						R_counter = R_counter + 1;
					end
					else	begin
						RNS = R_REC;
					end
			end
			default:	
						RNS = R_IDLE;
		endcase
	end
	always@(posedge CLOCK)	begin
		if(~RESET)
			RCS <= R_IDLE;
		else
			RCS <= RNS;
	end
endmodule
