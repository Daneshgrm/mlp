module SLP(OUT, IN, IN_SIZE, CLOCK, RESET, READY, INPUT_READY, tmp, 
    );

	localparam BIT_WIDTH = 16;
	localparam ADDR_WIDTH = 10;
	
	input wire CLOCK;
	input wire RESET;
	input wire [10:0]	IN_SIZE;
	input wire [BIT_WIDTH-1:0]	IN;
	input wire INPUT_READY;
	
	output reg READY;
	output reg [BIT_WIDTH-1:0] OUT;
	output wire [39:0] tmp;
	
	parameter READ = 1'b0, WRITE = 1'b1;
	parameter IDLE = 2'b00,R = 2'b01, W = 2'b10;
	
	reg [1:0] RCS;
	reg [1:0] RNS;
	
	reg [10:0] input_counter;
	reg [10:0] mac_counter;
	reg [9:0] read_addr;
	
	wire mode;
	reg start_ctrl;
	reg [ADDR_WIDTH-1:0] addr;
	reg [BIT_WIDTH-1:0] data_tmp;
	reg [BIT_WIDTH-1:0] data_tmp1;
	wire input_ready;
	
	
	wire busy;
	wire oe_ctrl;
	wire cs_ctrl;
	wire we_ctrl;
	wire ready_ctrl;
	wire [ADDR_WIDTH-1:0] addr_ctrl;
	wire [BIT_WIDTH-1:0] data_ctrl;
	wire [BIT_WIDTH-1:0] data;
	
	reg  [BIT_WIDTH-1:0] MP1;
	reg  [BIT_WIDTH-1:0]	MP2;
	reg  [BIT_WIDTH-1:0]	BIAS;
	wire  [39:0] MAC_OUT;
	reg  [39:0]				SUM;
	reg cordic_ready;
	reg  [BIT_WIDTH-1:0]	cordic_input;

	reg R_WE, R_OE, R_CS;
	
	Cordic cordic (.ZOUT(OUT), .CLOCK(CLOCK), .RESET(RESET), .ZIN(MAC_OUT));
		
	MAC mac(.clk(CLOCK), .a(16'b0000000000000001), .b(16'b0000000000000001), .c(SUM), .p(MAC_OUT));
	
	SRAM weight_ram(.CLOCK(CLOCK), .WE(R_WE), .CS(R_CS), .OE(R_OE),
			.ADDR(addr) , .DATA(data));
	
	assign input_ready = INPUT_READY;
	assign data = (mode == WRITE) ? data_tmp : 16'bz;
	assign mode = (input_counter == IN_SIZE) ? READ : WRITE;
	
	
	always@(posedge CLOCK)	begin
		if(~RESET)	begin
			RCS <= IDLE;
			MP1 <= 1;
			MP2 <= 2;
			start_ctrl <= 0;
			input_counter <= 0;
			read_addr <= 0;
			mac_counter <= 0;
			SUM <= 0;
			READY <= 1'b1;
		end
	end
	
	always@(posedge CLOCK)	begin
		case(RCS)
			IDLE:	begin
					R_OE <= 1'b0;
					R_WE <= 1'b0;
					R_CS <= 1'b0;
					READY <= 1'b1;
					if(input_ready)	begin
						if(mode == WRITE)	begin
							addr <= input_counter;
							input_counter <= input_counter + 1;
							data_tmp <= IN;
							RCS <= W;
						end
						else	begin
							if(mac_counter <= IN_SIZE)	begin
								addr <= mac_counter;
								mac_counter <= mac_counter + 1;
								RCS <= R;
								if(mac_counter == IN_SIZE)	begin
									BIAS <= data;
								end
								else
									MP1 <= data;
									MP2 <= IN;
									SUM <= SUM + MAC_OUT;
							end
							else
								mac_counter <= 0;
								cordic_input <= SUM + BIAS;
						end
					end
			end
			W:	begin
					R_OE <= 1'b0;
					R_WE <= 1'b1;
					R_CS <= 1'b1;
					RCS <= IDLE;
					READY <= 1'b0;
			end
			R:	begin
					R_OE <= 1'b1;
					R_WE <= 1'b0;
					R_CS <= 1'b1;
					RCS <= IDLE;
					READY <= 1'b0;
			end
		
		endcase
	end
//	assign tmp[18:17]	= RCS;
//	assign tmp[9:0]	= input_counter;
	assign tmp	= MAC_OUT;
//	assign tmp[26]		= ready_ctrl;
//	assign tmp[27]		= start;
//	assign tmp[28]    = we_ctrl;
//	assign tmp[29]		= oe_ctrl;
//	assign tmp[29]		= input_ready;
//	assign tmp[30]    = READY;
//	assign tmp[31]		= mode;

endmodule
module SRAM_CTRL(CLOCK, START, RW, ADDR, DATAIN, ADDR2R, 
	WE2R, OE2R, CS2R, DATA2R, BUSY, RESET, READY
);	
	localparam BIT_WIDTH = 16;
	localparam ADDR_WIDTH = 10;
	localparam RAM_DEPTH = 1 << 10;
	
	input wire CLOCK;
	input wire START;
	input wire RW;
	input wire RESET;
	input wire [ADDR_WIDTH-1:0] ADDR;
	
	inout wire [BIT_WIDTH-1:0] DATAIN;
	inout wire [BIT_WIDTH-1:0] DATA2R;
	
	output reg WE2R;
	output reg OE2R;
	output reg CS2R;
	output reg BUSY;
	output reg [ADDR_WIDTH-1:0] ADDR2R;
	output reg READY;
	
	parameter IDLE = 3'b000, W = 3'b001, R = 3'b010, NOP = 3'b011, FINISH = 3'b100;
	parameter READ = 1'b0, WRITE = 1'b1;
	reg mode;
	reg [2:0] RCS;
	reg [2:0] RNS;
	reg [BIT_WIDTH-1:0]	data_tmp;
	
	assign DATAIN = mode ? 16'bz : data_tmp;
	assign DATA2R = mode ? data_tmp : 16'bz;
//	assign READY = ~BUSY;
	
	always@(RCS, START, mode, BUSY)	begin
		case(RCS)
			IDLE:	begin
				if(START)	begin
						CS2R	= 1'b1;
						RNS = RW ? W : R;
						ADDR2R = ADDR;	
						READY = 1'b0;
				end
				else	begin
						CS2R	= 1'b0;
						RNS = IDLE;
				end
			end
			R:		begin
					data_tmp = DATA2R;
					mode = READ;
					RNS = NOP;
					BUSY = 1'b1;
			end
			W:		begin
					data_tmp = DATAIN;
					mode = WRITE;
					RNS = NOP;
					BUSY = 1'b1;
			end
			NOP:	begin
					if(mode == READ)	begin
						OE2R = 1'b1;
						WE2R = 1'b0;
						RNS = FINISH;
					end
					else	begin
						OE2R = 1'b0;
						WE2R = 1'b1;
						RNS = FINISH;
					end
			end
			FINISH:	begin
						BUSY = 1'b0;
						WE2R = 1'b0;
						OE2R = 1'b0;
						CS2R = 1'b0;
						READY = 1'b1;
						RNS = IDLE;
			end
		endcase
	end
	
	always@(posedge CLOCK)	begin
		if(~RESET)
			RCS <= IDLE;
		else
			RCS <= RNS;
	end
	always@(posedge CLOCK) begin
		if(~RESET)	begin
			OE2R <= 1'b0;
			CS2R <= 1'b0;
			WE2R <= 1'b0;
			BUSY <= 1'b0;
			READY <= 1'b1;
		end
	end
endmodule	

module SRAM(DATA, ADDR, CLOCK, CS, OE, WE
);
	
	localparam BIT_WIDTH = 16;
	localparam ADDR_WIDTH = 10;
	localparam RAM_DEPTH = 1 << 10;
	
	input wire CLOCK;
	input wire CS;
	input wire OE;
	input wire WE;
	
	input wire [ADDR_WIDTH-1:0]	ADDR;
	
	inout wire [BIT_WIDTH-1:0]	DATA;
	
	reg [BIT_WIDTH-1:0]	mem [0:RAM_DEPTH-1];
	reg [BIT_WIDTH-1:0] data_tmp;
	
	assign DATA = (CS & OE & !WE) ? data_tmp : 16'bz;
	
	always@(posedge CLOCK)	begin
		if(CS & WE)
			mem[ADDR] <= DATA;
		else if(CS & OE & !WE)
			data_tmp	 <= mem[ADDR];
	end
endmodule
