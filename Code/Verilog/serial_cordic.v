module serial_cordic(RX, TX, CLOCK, RESET
    );
	
	localparam bitWidth = 16;
	localparam DIVISOR = 4;
	localparam packetNo = 2;
	
	input wire CLOCK;
	input wire RESET;
	input wire	RX;
	
	output wire TX;
	
	parameter T_IDLE = 1'b0,	T_TRANS = 1'b1;
	parameter R_IDLE = 1'b0,	R_REC	=	1'b1;
	
	reg			TCS, TNS;
	reg			TX_enable;
	wire			TX_ready;
	reg	[1:0]	T_counter;
	reg	[7:0]	TX_data;
	reg	RCS, RNS;
	
	
	
	
	wire	[7:0]	RX_data;
	wire			RX_enable;
	
	
	reg	[1:0]	R_counter;
	wire			start_trans;
	
	reg 			input_ready;
	reg			output_ready;
	reg 	[bitWidth-1:0]	cordic_input;
	reg	[4:0]		cordicCounter;
	
	wire 	[bitWidth-1:0]	ZOUT;
	
	assign start_trans = output_ready & TX_ready;
	
	basic_uart #(.DIVISOR(DIVISOR))UART(.clk(CLOCK), .reset(~RESET), 
			.rx(RX), .rx_data(RX_data), .rx_enable(RX_enable),
			.tx_data(TX_data), .tx_enable(TX_enable), 
			.tx(TX), .tx_ready(TX_ready));	
	
	
	Cordic cordic (
		.ZOUT(ZOUT), 
		.CLOCK(CLOCK),
		.ZIN(cordic_input),
		.RESET(RESET)
	);
	
	always@(posedge CLOCK)	begin
		if(~RESET)	begin
			T_counter <= 0;
			R_counter <= 0;
			input_ready <= 0;
			output_ready <= 0;
		end
	end
	
	always@(RCS, RX_enable, R_counter)	begin
		case(RCS)
			R_IDLE:	begin
				if(RX_enable && R_counter < packetNo)	begin
					RNS		=	R_REC;
					cordic_input[R_counter*8 +:8]	= RX_data;
				end
				else if(R_counter == packetNo)	begin
					R_counter = 0;
					RNS = R_IDLE;
					input_ready = 1'b1;
				end
				else	begin
					RNS = R_IDLE;
				end	
			end
			R_REC:	begin
					if(~RX_enable)	begin
						RNS = R_IDLE;
						R_counter = R_counter + 1;
					end
					else	begin
						RNS = R_REC;
					end
			end
			default:	
						RNS = R_IDLE;
		endcase
	end
	always@(posedge CLOCK)	begin
		if(~RESET)
			RCS <= R_IDLE;
		else
			RCS <= RNS;
	end
	
	always@(posedge CLOCK)	begin
		if(cordicCounter == 31)
			output_ready <= 1;
		else
			cordicCounter <= input_ready ? cordicCounter + 1 : 0;
	end
	
	always@(TCS, T_counter, start_trans, TX_ready, cordicCounter)	begin
		case(TCS)
			T_IDLE:	begin
				if(start_trans && T_counter < packetNo && output_ready )	begin
						TNS = T_TRANS;
						TX_data = ZOUT[T_counter*8 +: 8];
						TX_enable = 1'b1;
				end
				else if(T_counter == packetNo)	begin
						TNS	= T_IDLE;
						T_counter = 0;
						output_ready = 0;
				end
				else	begin
						TNS = T_IDLE;
				end
			end
			T_TRANS:	begin
						TX_enable = 1'b0;
					if(TX_ready)	begin
						TNS = T_IDLE;
						T_counter = T_counter + 1;
					end
					else	begin
						TNS = T_TRANS;
					end
			end
			default:
						TNS = T_IDLE;
		endcase
	end
	
	always@(posedge CLOCK)	begin
		if(~RESET)
			TCS <= T_IDLE;
		else
			TCS <= TNS;
	end
endmodule
