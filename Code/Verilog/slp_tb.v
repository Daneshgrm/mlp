`timescale 1ns / 1ps
module slp_test;

	// Inputs
	
	
	reg [15:0] IN;
	reg [15:0] slp_input;
	reg input_serial_start;
	reg slp_serial_start;
	reg [10:0] IN_SIZE;
	reg INPUT_READY;
	reg CLOCK;
	reg RESET;
	
	// Outputs
	wire [15:0] OUT;
	wire [15:0] slp_output;
	wire READY;
	wire [31:0]	out_tmp;
	wire test;
	
	wire RX;
	wire TX;
	wire input_serial_rdy;
	wire input_serial_inrdy;
	wire serial_slp_rdy;
	wire serial_slp_inrdy;
	wire get_input;
	
	integer op_weight, op_in, op_out, i, tmp, counter, input_counter;
	
	// Instantiate the Unit Under Test (UUT)
	
	serial_tmp input_serial(
		.RX(RX),
		.TX(TX),
		.OUT(OUT),
		.IN(IN),
		.CLOCK(CLOCK),
		.RESET(RESET),
		.START(input_serial_start),
		.RDY(input_serial_rdy),
		.IN_RDY(input_serial_inrdy)
	);
	
	serial_tmp serial_slp(
		.RX(TX),
		.TX(RX),
		.OUT(slp_input),
		.IN(slp_output),
		.CLOCK(CLOCK),
		.RESET(RESET),
		.START(slp_serial_start),
		.RDY(serial_slp_rdy),
		.IN_RDY(serial_slp_inrdy)
	);
	SLP uut (
		.OUT(slp_output), 
		.IN(slp_input), 
		.IN_SIZE(IN_SIZE), 
		.CLOCK(CLOCK), 
		.RESET(RESET),
		.READY(READY),
		.INPUT_READY(serial_slp_ready),
		.tmp(out_tmp)
	);

	initial begin
		// Initialize Inputs
		IN_SIZE = 10'b0000001000;
		CLOCK = 0;
		RESET = 0;
		counter = 0;
		input_counter = 0;
		input_serial_start = 1'b1;
		
		op_weight = $fopen("weight.txt", "r");
		op_in  = $fopen("in.txt", "r");
		op_out = $fopen("out.txt", "w");
		
		// Wait 100 ns for global reset to finish
		#50 RESET = 1;
		// Add stimulus here
	end
   assign get_input = counter > IN_SIZE;
	always #5 CLOCK = ~CLOCK;
	always@(posedge CLOCK)	begin
		input_serial_start <= serial_slp_inrdy; 
	end
	always@(posedge CLOCK)	begin
			if(serial_slp_inrdy && RESET && counter <= IN_SIZE)	begin
				tmp <= $fscanf(op_weight, "%b\n", IN);
				counter = counter + 1;
			end
			else if(counter > IN_SIZE)	begin
				$fclose(op_weight);
			end
	end
	always@(posedge CLOCK)	begin
		if(get_input && input_counter < IN_SIZE && serial_slp_inrdy)	begin
				tmp <= $fscanf(op_in, "%b\n", IN);
				input_counter = input_counter + 1;
		end
		else if(input_counter == IN_SIZE)
				input_counter = 0;
	
	end
endmodule

