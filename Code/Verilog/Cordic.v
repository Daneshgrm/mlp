module Cordic(ZOUT, CLOCK, ZIN, RESET
	);
	
	localparam bitWidth = 16;
	
	input wire CLOCK;
	input wire RESET;
	input wire signed [bitWidth-1:0] ZIN;
	
	output wire signed  [bitWidth-1:0] ZOUT;
	
	wire signed [bitWidth:0]	xtmp1;
	wire signed [bitWidth:0]	ytmp1;
	
	
	CordicHyper cordicHyper(.XOUT(xtmp1), .YOUT(ytmp1), .CLOCK(CLOCK), 
			.ZIN(ZIN), .RESET(RESET));
	CordicLinear cordicLinear(.ZOUT(ZOUT), .CLOCK(CLOCK), .XIN(xtmp1), .YIN(ytmp1), 
			.RESET(RESET));
endmodule
module CordicLinear(ZOUT, CLOCK, XIN, YIN, RESET
	);

	localparam bitWidth = 16;
	localparam posIterNo = 12;
	localparam stagesNo = posIterNo;
	integer i;
	
	input wire CLOCK;
	input wire RESET;
	input wire signed [bitWidth:0] XIN;
	input wire signed [bitWidth:0] YIN;
	
	output wire signed [bitWidth-1:0] ZOUT;

	
	reg signed [bitWidth:0] X [0:stagesNo];
	reg signed [bitWidth:0] Y [0:stagesNo];
	reg signed [bitWidth-1:0] Z [0:stagesNo];
	
	always@(posedge CLOCK)	begin
		if(~RESET)	begin
			Z[0] <= 16'b00000000_00000000;
			X[0] <= 17'b0_00000000_00000000;
			Y[0] <= 17'b0_00000000_00000000;
		end
		else begin
			X[0] <= XIN;
			Y[0] <= YIN;
			for(i = 1; i <= stagesNo ; i=i+1)
			begin
				
				X[i] <= X[i-1];
				Y[i] <= Y[i-1][bitWidth] ? Y[i-1] + (X[i-1] >>> i):
												Y[i-1] - (X[i-1] >>> i);
				Z[i] <= Y[i-1][bitWidth] ? Z[i-1] - (16'b001_0000000000000 >>> i):
												Z[i-1] + (16'b001_0000000000000 >>> i);
			end
		end
	end
	assign ZOUT = Z[stagesNo];


endmodule

module CordicHyper(XOUT, YOUT, CLOCK, ZIN, RESET
    );
		
	localparam negIterNo = 3;
	localparam posIterNo = 13;
	localparam bitWidth = 16;
	localparam stagesNo = negIterNo + posIterNo + 1; // positive + negative iterations + repetitions
	localparam repetition_1 = 4;
	
	
	integer i, k;
	
	input wire CLOCK;
	input wire RESET;
	input wire signed [bitWidth-1:0] ZIN;
	
	output wire signed [bitWidth:0] XOUT;
	output wire signed [bitWidth:0] YOUT;
	
	
	reg signed [bitWidth:0] X [0:stagesNo];
	reg signed [bitWidth:0] Y [0:stagesNo];
	reg signed [bitWidth-1:0] Z [0:stagesNo];
	
	parameter [bitWidth:0] XIN1 = 17'b0_00101011_01011000;
	parameter [bitWidth:0] YIN1 = 17'b0_00000000_00000000;
	
	wire signed [bitWidth-1:0] LUT [0:stagesNo-1];
	
	assign LUT[0] 	=	16'b001_1011011110010;
	assign LUT[1] 	=	16'b001_0101101010100;
	assign LUT[2] 	=	16'b000_1111100100010;
	
	assign LUT[3] 	=	16'b000_1000110010100;
	assign LUT[4] 	=	16'b000_0100000101100;
	assign LUT[5] 	=	16'b000_0010000000101;
	
	assign LUT[6] 	=	16'b000_0001000000001;
	assign LUT[7] 	=	16'b000_0001000000001;
	
	assign LUT[8] 	=	16'b000_0000100000000;
	assign LUT[9] 	=	16'b000_0000010000000;
	assign LUT[10] =	16'b000_0000001000000;
	assign LUT[11] =	16'b000_0000000100000;
	assign LUT[12] =	16'b000_0000000010000;
	assign LUT[13] = 	16'b000_0000000001000;
	assign LUT[14] = 	16'b000_0000000000100;
	assign LUT[15] = 	16'b000_0000000000010;
	assign LUT[16] = 	16'b000_0000000000001;
	

	always@(posedge CLOCK)	begin
		if(~RESET)	begin
			X[0] <= XIN1;
			Y[0] <= YIN1;
			Z[0] <= 16'b0;
		end
		else	begin
			Z[0] <= ZIN;
		end
	end
	always@(posedge CLOCK)	begin
			for(i = 1 ; i <= negIterNo ; i=i+1)
				begin
				
					X[i] <= Z[i-1][bitWidth-1] ? X[i-1] - (Y[i-1] - (Y[i-1] >>> (negIterNo-i+2))):
												X[i-1] + (Y[i-1] - (Y[i-1] >>> (negIterNo-i+2)));
					Y[i] <= Z[i-1][bitWidth-1] ? Y[i-1] - (X[i-1] - (X[i-1] >>> (negIterNo-i+2))):
												Y[i-1] + (X[i-1] - (X[i-1] >>> (negIterNo-i+2)));
					Z[i] <= Z[i-1][bitWidth-1] ? Z[i-1] + LUT[i-1] : Z[i-1] - LUT[i-1];

				end
			for(i = negIterNo+1 ; i <= negIterNo+3 ; i=i+1)
				begin
					
						X[i] <= Z[i-1][bitWidth-1] ? X[i-1] - (Y[i-1] >>> (i-negIterNo)):
													X[i-1] + (Y[i-1] >>> (i-negIterNo));
						Y[i] <= Z[i-1][bitWidth-1] ? Y[i-1] - (X[i-1] >>> (i-negIterNo)):
													Y[i-1] + (X[i-1] >>> (i-negIterNo));
						Z[i] <= Z[i-1][bitWidth-1] ? Z[i-1] + LUT[i-1] : Z[i-1] - LUT[i-1];
					
				end
			for(i = negIterNo+repetition_1+2 ; i <= stagesNo ; i=i+1)
				begin
					
						X[i] <= Z[i-1][bitWidth-1] ? X[i-1] - (Y[i-1] >>> (i-negIterNo-1)):
													X[i-1] + (Y[i-1] >>> (i-negIterNo-1));
						Y[i] <= Z[i-1][bitWidth-1] ? Y[i-1] - (X[i-1] >>> (i-negIterNo-1)):
													Y[i-1] + (X[i-1] >>> (i-negIterNo-1));
						Z[i] <= Z[i-1][bitWidth-1] ? Z[i-1] + LUT[i-1] : Z[i-1] - LUT[i-1];
					
				end
			for(i = 0 ; i < 2 ; i=i+1)
				begin
					k = i+negIterNo+repetition_1;
					X[k] <= Z[k-1][bitWidth-1] ? X[k-1] - (Y[k-1] >>> (repetition_1)):
												X[k-1] + (Y[k-1] >>> (repetition_1));
					Y[k] <= Z[k-1][bitWidth-1] ? Y[k-1] - (X[k-1] >>> (repetition_1)):
												Y[k-1] + (X[k-1] >>> (repetition_1));
					Z[k] <= Z[k-1][bitWidth-1] ? Z[k-1] + LUT[k-1] : Z[k-1] - LUT[k-1];
				end	
	end
		
	assign XOUT = X[stagesNo];
	assign YOUT = Y[stagesNo];
endmodule
