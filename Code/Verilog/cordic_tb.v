`timescale 1ns / 1ps
module CordicTest;

	// Inputs
	reg CLOCK;
	reg RESET;
	reg signed [15:0] ZIN;

	// Outputs
	wire signed[15:0] ZOUT;
	integer op1, op_out, tmp, k, counter;
	
	// Instantiate the Unit Under Test (UUT)
	Cordic uut (
		.ZOUT(ZOUT), 
		.CLOCK(CLOCK),  
		.ZIN(ZIN), 
		.RESET(RESET)
	);

	initial begin
		// Initialize Inputs
		CLOCK = 0;
		k = 0;
		counter = 0;
		ZIN = 0;
		RESET = 0;
		op1 = $fopen("test.txt", "r");
		op_out = $fopen("result.txt", "w");
		
		#50	RESET = 1'b1;
		
	end
	always #5 CLOCK <= ~CLOCK;
	always@(posedge CLOCK)	begin
		if(RESET)
			tmp <= $fscanf(op1, "%b\n", ZIN);
	end
	always@(posedge CLOCK)	begin
		if(RESET)	begin
			if(k >= 32)	begin
				if(counter <= 1024)	begin
					$fwrite(op_out, "%b\n", ZOUT);
					counter = counter + 1;
				end
				else begin
					$fclose(op1);
					$fclose(op_out);
					$stop();
				end
			end
			else
				k = k + 1;
		end
	end
endmodule

