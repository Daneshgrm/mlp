module serial_test;

	// Inputs
	wire RX;
	reg [15:0] IN1;
	reg CLOCK;
	reg RESET;
	reg START;
	wire TX;
	wire [15:0] OUT1;
	
	reg [15:0] IN2;
	wire [15:0] OUT2;

	// Instantiate the Unit Under Test (UUT)
	serial uut1 (
		.RX(RX), 
		.TX(TX), 
		.OUT(OUT1), 
		.IN(IN1), 
		.CLOCK(CLOCK), 
		.RESET(RESET), 
		.START(START)
	);
	
	serial uut2 (
		.RX(TX), 
		.TX(RX), 
		.OUT(OUT2), 
		.IN(IN2), 
		.CLOCK(CLOCK), 
		.RESET(RESET), 
		.START(START)
	);

	initial begin
		// Initialize Inputs
		IN1 = 16'b01100110_10011001;
		IN2 = 16'b00011100_11100011;
		CLOCK = 0;
		RESET = 0;
		START = 1;

		// Wait 100 ns for global reset to finish
		#20	RESET = 1;
        
		// Add stimulus here

	end
   always #5	CLOCK = ~CLOCK;
endmodule

