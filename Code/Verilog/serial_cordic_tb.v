`timescale 1ns / 1ps
module serial_cordic_test;

	// Inputs
	wire RX;
	wire TX;
	reg CLOCK;
	reg RESET;
	reg START;
	reg [15:0]	IN;
	wire [15:0]	OUT;

	// Instantiate the Unit Under Test (UUT)
	serial_cordic uut_cordic (
		.RX(TX), 
		.TX(RX), 
		.CLOCK(CLOCK), 
		.RESET(RESET)
	);
	
	serial uut_serial (
		.RX(RX), 
		.TX(TX), 
		.OUT(OUT), 
		.IN(IN), 
		.CLOCK(CLOCK), 
		.RESET(RESET), 
		.START(START)
	);
	initial begin
		// Initialize Inputs
		CLOCK = 0;
		RESET = 0;
		START = 1;
		IN = 16'b1000111111000000;
		// Wait 100 ns for global reset to finish
		#20;	RESET = 1;
        
		// Add stimulus here

	end
      
	always #5 CLOCK = ~CLOCK;
endmodule

