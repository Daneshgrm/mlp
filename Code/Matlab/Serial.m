%%
    delete(instrfind);
    serialOne = serial('COM13', 'BaudRate', 4800, 'StopBits', 1, 'DataBits', 8, 'Parity', 'none');
    serialOne.Terminator = 13;
    
    serialOne.ReadAsyncMode = 'continuous';
    serialOne.BytesAvailableFcnMode = 'terminator';
    serialOne.BytesAvailableFcn = {@receiveCallBack, bitWidth};
    
    fopen(serialOne);
    disp('Port is Open');
    
    data = input('Please enter an integer: ');
    bitWidth = 16;
    partitionNo = 2;
    data_fi = fi(data, 1, bitWidth, 0);
    data_fi = bin(data_fi);
    data_fi = reshape(data_fi, [bitWidth/2, partitionNo]);
    data_fi = data_fi';
    for i=1:size(data_fi, 1)
        fwrite(serialOne, data_fi(i, :));
    end

    
    function receiveCallBack(ser, ~, bitWidth)
        data = fscanf(ser);
        if(length(data) == bitWidth+1)
            data = bin2dec(data(1:bitWidth));
            fprintf('Received integer is %d \n', data);
        end
    end