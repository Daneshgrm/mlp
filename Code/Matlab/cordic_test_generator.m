%%
    zin = -4:2^-7:4;
    zNT = numerictype(zin, 1, 16, 13);
    zin_fi = fi(zin, zNT);
    fileID = fopen('test.txt', 'w+');
    test_data = cell2mat(strsplit(bin(zin_fi))');
    for i=1:length(zin_fi)
        fprintf(fileID, '%s\n', test_data(i, :));
    end