%%
    bitWidth  = 16;

    zin = -4:(2^-7):4;
    zin_fi  = fi(zin, 1, bitWidth, 13);

    zNT   = numerictype(zin_fi);
    xyNT   = numerictype(1, bitWidth+1, 10);
    xout   = fi(zeros(size(zin)), xyNT);
    yout   = fi(zeros(size(zin)), xyNT);
    zout   = fi(zeros(size(zin)), zNT);

    posIters = 13;
    negIters = 3;
    tmp = [1:posIters, 4];
    LUT = [fi(atanh(1-2.^((-negIters+1:0)-2)), zNT), fi(atanh(2.^-(1:posIters)), zNT)];
    An = prod([sqrt(1-(1-2.^((-negIters+1:0)-2)).^2), sqrt(1-2.^(-2*(tmp))) ]);
    for i = 1:length(zin)
        [xout(i), yout(i), zout(i)] = ...
            cordicHyper(fi(1/An, xyNT), fi(0, xyNT), zin_fi(i), ...
            LUT, posIters, negIters, [xyNT, zNT]);
        [xout(i), yout(i), zout(i)] = ...
            cordicLinear(fi(xout(i), xyNT), fi(yout(i), xyNT), fi(0, zNT), ...
            12, [xyNT, zNT]);
    end
    figure;
    plot(zin, zout);
    title("Cordic Output");
    figure;
    plot(zin, tanh(zin));
    title("Tanh Output");
    
    figure;
    plot(abs(zout-tanh(zin)));








function [Xout, Yout, Zout] = cordicHyper(Xin, Yin, Zin, LUT, posIter, negIter, numericType)
    
    ctr = 1;
    Xout = fi(Xin, numericType(1));
    Yout = fi(Yin, numericType(1));
    Zout = fi(Zin, numericType(2));
    while(ctr <= posIter+negIter)
        Xtmp = Xout;
        Ytmp = Yout;
        Ztmp = Zout;
        if(ctr- negIter<= 0)
            if(Zout < 0)
                Xout = accumneg(Xtmp, accumneg(Ytmp, bitsra(Ytmp, negIter-ctr+2)));
                Yout = accumneg(Ytmp, accumneg(Xtmp, bitsra(Xtmp, negIter-ctr+2)));
                Zout = accumpos(Ztmp, LUT(ctr));
            else
                Xout = accumpos(Xtmp, accumneg(Ytmp, bitsra(Ytmp, negIter-ctr+2)));
                Yout = accumpos(Ytmp, accumneg(Xtmp, bitsra(Xtmp, negIter-ctr+2)));
                Zout = accumneg(Ztmp, LUT(ctr));
            end
        else
            if(Zout < 0)
                Xout = accumneg(Xtmp, bitsra(Ytmp, ctr-negIter));
                Yout = accumneg(Ytmp, bitsra(Xtmp, ctr-negIter));
                Zout = accumpos(Ztmp, LUT(ctr));
            else
                Xout = accumpos(Xtmp, bitsra(Ytmp, ctr-negIter));
                Yout = accumpos(Ytmp, bitsra(Xtmp, ctr-negIter));
                Zout = accumneg(Ztmp, LUT(ctr));
            end
        end
        if((ctr-negIter == 4) && repeat_flag == false)
            repeat_flag = true;
        else
            repeat_flag = false;
            ctr = ctr + 1;
        end
    end
        

end

function [Xout, Yout, Zout] = cordicLinear(Xin , Yin, Zin, nIters, numericType)
    
    Xout = fi(Xin, numericType(1));
    Yout = fi(Yin, numericType(1));
    Zout = fi(Zin, numericType(2));
    
    for i = 1:nIters
            if(Yout < 0)
                Yout = accumpos(Yout, bitsra(Xout, i));
                Zout = accumneg(Zout, bitsra(fi(1, numericType(2)), i));
            else
                Yout = accumneg(Yout, bitsra(Xout, i));
                Zout = accumpos(Zout, bitsra(fi(1, numericType(2)), i));
            end
    end
    
end